import React, { Component } from 'react';
import './Home.css';
import *  as firebase from 'firebase';




class Home extends Component {
constructor()
{
  super();
  this.state = {
    Counter:0 ,
    date:null
  };
}


componentDidMount() {

 

  const firebaseRef = firebase.database().ref('/FormData') ;

  firebaseRef.remove();

  const dataForm = {
    counter : this.state.Counter ,
    Date : this.state.date
   }; 

  firebaseRef.push(dataForm);

  this.interval = setInterval(() => {
 
    this.setState({
      Counter: this.state.Counter + 1 ,
       date: new Date().toLocaleString()
      });
      
      const formdata = {
       counter : this.state.Counter ,
       Date : this.state.date
      }; 

      firebaseRef.push(formdata);
  }, 5000);

}


componentWillUnmount() {
  clearInterval(this.interval);
}

  render() {
    return (
      <div className="Home">
        <div className="counterBox">
          <p className="counterNum">
          {this.state.Counter}
          </p>
        </div>

      </div>
    );
  }
}

export default Home;
