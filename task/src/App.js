import React, { Component } from 'react';
import { BrowserRouter , Route } from 'react-router-dom';
import Home from './Components/Home';
import List from './Components/List';
import LinkPages from './Components/LinkPages';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="App">
      <LinkPages />
      <Route exact path="/" component= {Home} />
      <Route  path="/List" component= {List} />   
      </div>

      </BrowserRouter>
    );
  }
}

export default App;
