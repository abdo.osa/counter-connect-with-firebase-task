import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';


var config = {
    apiKey: "AIzaSyCIrKhCg6R5kVkFIggO7PNwMJ6MvvO3SQk",
    authDomain: "react-task3.firebaseapp.com",
    databaseURL: "https://react-task3.firebaseio.com",
    projectId: "react-task3",
    storageBucket: "react-task3.appspot.com",
    messagingSenderId: "606536647152"
  };
  firebase.initializeApp(config);

 
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
