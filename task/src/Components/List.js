import React, { Component } from 'react';
import *  as firebase from 'firebase';



class List extends Component {

    constructor()
      {
        super();
        this.state = {
          currentItem: '',
          username: '',
          Data: []
        };
      }

    componentDidMount()
    {


        const Ref = firebase.database().ref('FormData');
        Ref.on('value', (snapshot) => {
        let Data = snapshot.val();
        let newState = [];

        for (let count in Data) {
          newState.push({
            id: count,
            Counter: Data[count].counter,
            Date: Data[count].Date
          });
        }
        this.setState({
          Data: newState
        });


            });
          
    }

    

  render() {
    return (
  
      <div className="List">

<ul>
      {this.state.Data.map((count) => {
        return (
          <li key={count.id}>
            <h3>
              Counter :  {count.Counter} 
              &nbsp;  
              &nbsp;
               Date => {count.Date}
               </h3>
          </li>
        )
      })}
    </ul>

      </div> 
    );
  }
}

export default List;
